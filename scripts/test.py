#!/usr/bin/env python
from hammer.hammerlib import *
from math import *
from ROOT import TFile, TChain, gROOT, AddressOf, vector
from array import array
import numpy as np
import math
from collections import OrderedDict

#Use structs members as targets for SetBranchAddress via AddressOf
gROOT.ProcessLine("""
struct particle_info
{
	double P_E, P_X, P_Y, P_Z;
	int ID;
};
""")

from ROOT import particle_info

def getMissingFourMomentum(parent, daughters, massless = False):
	"""
		Returns the four-momentum of a massless particle whose three-momentum is the difference between that of the parent and the listed daughters.
		The four-momentum can be forced to be massless.
	"""
	m = particle_info(parent)
	for d in daughters:
		m.P_E -= d.P_E
		m.P_X -= d.P_X
		m.P_Y -= d.P_Y
		m.P_Z -= d.P_Z
	if massless:
		m.P_E = math.sqrt(m.P_X**2 + m.P_Y**2 + m.P_Z**2)
	return m

def addParticles(ham, particle_data):
	allparticles = {}
	for name in particle_data:
		allparticles[name] = ham.addParticle(particle_data[name].ID, particle_data[name].P_E, particle_data[name].P_X, particle_data[name].P_Y, particle_data[name].P_Z)
	return allparticles

def addVertices(ham, allparticles, process_defs):
	for parent,daughters in process_defs:
		ham.addVertex(allparticles[parent], [allparticles[d] for d in daughters])

def main(args):
	#Configure the Hammer object
	ham = PyHammer()
	ham.includeDecay(["BD*TauNu"])
	ham.setUnits("MeV")
	ham.addFFScheme("CLN_Scheme", {"BD*": "CLN"})
	ham.setFFInputScheme({"BD*": "ISGW2"})
	ham.initRun()
	CLNparams = {
		"RhoSq" : 1.205,
		"R0" : 1.140,
		"R1" : 1.404,
		"R2" : 0.854,
	}
	ham.setOptions("BtoD*CLN: {}".format(str(CLNparams).replace("'","")))
	#Input MC
	inputtree = TChain(args.path)
	for f in args.file:
		inputtree.Add(f)
	#New output file with Hammer weights added as a branch
	if args.output == None:
		outfile_name = args.file[0].replace(".root", "_hamweights.root")
	else:
		outfile_name = args.output
	outfile = TFile(outfile_name,"RECREATE")
	outtree = inputtree.CloneTree(-1)
	hamweight = np.array([1.])
	weight_branch = outtree.Branch('hamweight',hamweight,'hamweight/D')
	particle_data = OrderedDict()
	#Branch names in the nTuple
	for name in ["B0", "Dst", "tau"]:
		particle_data[name] = particle_info()
		for var in ["P_E", "P_X", "P_Y", "P_Z", "ID"]:
			inputtree.SetBranchAddress(name+"_TRUE"+var, AddressOf(particle_data[name], var))
	n = inputtree.GetEntries()
	for i in range(n):
		ham.initEvent()
		hamweight[0] = 0
		inputtree.GetEntry(i)
		if i % 1000 == 0:
			print("{}/{}".format(i,n))
		# Account for oscillated B0's
		if particle_data["B0"].ID * particle_data["tau"].ID > 0:
			particle_data["B0"].ID *= -1;
		# Figure out the "neutrino" four-momenta
		# TODO: includes radiative photons --- how much of a problem is this?
		particle_data["nu"] = getMissingFourMomentum(particle_data["B0"], [particle_data["Dst"], particle_data["tau"]], False);
		particle_data["nu"].ID = 16 if particle_data["tau"].ID<0 else -16;
		ham.initProcess() # Need this before doing addParticle
		allparticles = addParticles(ham, particle_data)
		print(allparticles)
		processes = [
			("B0", ["Dst", "tau", "nu"]),
		]
		addVertices(ham, allparticles, processes)
		procID = ham.addProcess()
		if(procID != 0):
			ham.processEvent()
			hamweight[0] = ham.getWeight("CLN_Scheme", [procID])
			if np.isnan(hamweight[0]):
				print("Hammer weight is NaN")
		weight_branch.Fill()
	#Save the output tree
	outfile.cd()
	outtree.AutoSave()
	outfile.Write()
	outfile.Close()

if __name__ == "__main__":
	import argparse
	parser = argparse.ArgumentParser(description="Reweight B->D* tau nu form factor in simulation sample(s) using HAMMER")
	parser.add_argument("file", nargs="+", help="sample to reweight")
	parser.add_argument("--path", default="DecayTree", help="path to ntuple in each file")
	parser.add_argument("--output", default=None, help="output filename")
	args = parser.parse_args()
	main(args)

