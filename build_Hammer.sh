#!/bin/bash
install_dir=~/.local/hammer
src_folder=Hammer
mkdir -p build && cd build && \
cmake ../$src_folder -DCMAKE_INSTALL_PREFIX=$install_dir -DWITH_ROOT=OFF -DWITH_PYTHON=ON -DWITH_EXAMPLES=OFF -DBUILD_DOCUMENTATION=OFF -DENABLE_TESTS=OFF -DINSTALL_EXTERNAL_DEPENDENCIES=OFF -DMAX_CXX_IS_14=OFF -DCMAKE_INSTALL_LIBDIR=lib && \
make -j && \
make install && \
echo "Hammer installed to $install_dir" && \
echo "
Run this once to create a viritualenv:
    python -m venv --system-site-packages $install_dir
Then run this each time to active it
    source $install_dir/bin/activate
"


