#include "Hammer/Hammer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/ProcManager.hh"
#include "Hammer/Math/FourMomentum.hh"

#include "TFile.h"
#include "TTree.h"

#include <vector>
#include <string>
#include <iostream>

struct particle_info { double E, PX, PY, PZ; int PID; };

particle_info getMissingFourMomentum(particle_info parent, std::vector<particle_info> daughters)
{
	particle_info missing = parent;
	for(auto&& d: daughters)
	{
		missing.E -= d.E;
		missing.PX -= d.PX;
		missing.PY -= d.PY;
		missing.PZ -= d.PZ;
	}
	return missing;
}

int main()
{
	Hammer::Hammer ham{};
	ham.includeDecay(std::vector<std::string>{"BD*TauNu", "D*DPi"});
	ham.addFFScheme("Scheme1", {{"BD*", "CLN"}});
	ham.setFFInputScheme({{"BD*", "ISGW2"}});
	ham.initRun();
	auto intree = static_cast<TTree*>(TFile::Open("11160001_MC16_strip_truth.root")->Get("DecayTreeTuple/DecayTree"));
	auto outfile = TFile::Open("weighted.root", "RECREATE");
	auto tree = intree->CloneTree(-1);
	double eventweight;
	tree->Branch("hamweight", &eventweight, "hamweight/D");
	std::map<std::string, particle_info> particle_data;
	std::vector<std::string> allnames{"B0", "Dst", "D0", "dst_pion", "dz_kaon", "dz_pion", "tau", "tau_pion0", "tau_pion1", "tau_pion2"};
	for(auto&& name: allnames)
	{
		particle_data[name] = particle_info();
		tree->SetBranchAddress((name+"_TRUEP_E").c_str(), &particle_data[name].E);
		tree->SetBranchAddress((name+"_TRUEP_X").c_str(), &particle_data[name].PX);
		tree->SetBranchAddress((name+"_TRUEP_Y").c_str(), &particle_data[name].PY);
		tree->SetBranchAddress((name+"_TRUEP_Z").c_str(), &particle_data[name].PZ);
		tree->SetBranchAddress((name+"_TRUEID" ).c_str(), &particle_data[name].PID);
	}
	unsigned n = tree->GetEntries();
	for(unsigned i = 0; i < n; ++i)
	{
		tree->GetEntry(i);
		// Convert from MeV to GeV
		for(auto&& name: allnames)
		{
			particle_data[name].E /= 1000.;
			particle_data[name].PX /= 1000.;
			particle_data[name].PY /= 1000.;
			particle_data[name].PZ /= 1000.;
		}
		// Account for oscillated B0's
		if(particle_data["B0"].PID * particle_data["tau"].PID > 0)
		{
			particle_data["B0"].PID *= -1;
		}
		ham.initEvent();
		// Figure out the "neutrino" four-momenta
		// TODO: includes radiative photons --- how much of a problem is this?
		particle_data["nu1"] = getMissingFourMomentum(particle_data["B0"], {particle_data["Dst"], particle_data["tau"]});
		particle_data["nu1"].PID = particle_data["tau"].PID<0 ? 16 : -16;
		particle_data["nu2"] = getMissingFourMomentum(particle_data["tau"], {particle_data["tau_pion0"], particle_data["tau_pion1"], particle_data["tau_pion2"]});
		particle_data["nu2"].PID = -particle_data["nu1"].PID;
		std::map<std::string, Hammer::ParticleIndex> allparticles;
		Hammer::Process proc;
		for(auto&& list: {allnames,{"nu1", "nu2"}})
		{
			for(auto&& name: list)
			{
				Hammer::FourMomentum momentum{particle_data[name].E, particle_data[name].PX, particle_data[name].PY, particle_data[name].PZ};
				if(name.find("nu") != std::string::npos)
				{
					momentum.setE(momentum.p());
				}
				allparticles[name] = proc.addParticle(Hammer::Particle{momentum, particle_data[name].PID});
			}
		}
		std::vector<std::pair<std::string, std::vector<std::string>>> processtree = {
			{"B0", {"Dst", "tau", "nu1"}},
			{"Dst", {"D0", "dst_pion"}},
			{"D0", {"dz_kaon", "dz_pion"}},
			{"tau", {"tau_pion0", "tau_pion1", "tau_pion2", "nu2"}}
		};
		for(auto&& desc: processtree)
		{
			size_t parent = allparticles[desc.first];
			std::vector<size_t> daughters;
			for(auto&& name: desc.second)
			{
				daughters.push_back(allparticles[name]);
			}
			proc.addVertex(parent, daughters);
			std::cout << parent << " -> [";
			for(auto&& d: daughters) std::cout << d << ", ";
			std::cout << "]\n";
		}
		auto procID = ham.addProcess(proc);
		if(procID != 0)
		{
			ham.processEvent();
			std::vector<size_t> procs = {procID};
			eventweight = ham.getWeight("Scheme1");
			std::cout << eventweight << "\n";
		}
		else
		{
			std::cout << "Forbidden event\n";
			eventweight = 0;
		}
		if(std::isnan(eventweight) || std::isinf(eventweight))
		{
			std::cout << "Event weight is " << eventweight << "\n";
			eventweight = 0;
		}
		tree->Fill();
	}
	tree->Write();
	outfile->Close();
	return 0;
}
