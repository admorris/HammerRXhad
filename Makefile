# Compiler and shell
CC         = g++
SHELL      = /bin/bash
RM         = rm -f

# Extensions
SRCEXT     = cpp
HDREXT     = h
OBJEXT     = o

# Directories
HDRDIR     = include
OBJDIR     = build
SRCDIR     = src
BINDIR     = bin

# List of source files and objects
SRCS      := $(shell find $(SRCDIR) -name '*.$(SRCEXT)')
HDRS      := $(shell find $(HDRDIR) -name '*.$(HDREXT)')
OBJS      := $(patsubst $(SRCDIR)/%.$(SRCEXT), $(OBJDIR)/%.$(OBJEXT), $(SRCS))
BINS      := $(patsubst $(SRCDIR)/%.$(SRCEXT), $(BINDIR)/%, $(SRCS))

# ROOT
ROOTCFLAGS     = $(shell root-config --cflags)
ROOTLIBS       = $(shell root-config --libs)
ROOTLIBDIR     = $(shell root-config --libdir)
ROOTLIBFLAGS   = -L$(ROOTLIBDIR) -Wl,--as-needed $(ROOTLIBS) -Wl,-rpath,$(ROOTLIBDIR)

# Submodules
HAMLIB      = $HOME/.local/src/Hammer/build/lib/hammerlib.so
HAMLIBFLAGS = -Wl,--as-needed $(HAMLIB) -Wl,-rpath,$(HAMLIBDIR)

# Compiler flags
CXXFLAGS       = -Wall -fPIC -I$(HDRDIR) $(ROOTCFLAGS) $(HAMCXXFLAGS)
LIBFLAGS       = -Wl,--no-undefined -Wl,--no-allow-shlib-undefined $(ROOTLIBFLAGS) $(HAMLIBFLAGS)

.PHONY: all clean
all: $(BINS)
clean:
	@$(RM) $(OBJS) $(BINS)
# Create necessary folders
$(OBJDIR) $(BINDIR):
	@mkdir -p $@
# Build binaries
$(BINDIR)/% : $(OBJDIR)/%.$(OBJEXT) $(HAMLIB) | $(BINDIR)
	@echo "Linking $@"
	@$(CC) $< -o $@ $(LIBFLAGS)
# Build objects
$(OBJDIR)/%.$(OBJEXT) : $(SRCDIR)/%.$(SRCEXT) $(HDRS) | $(OBJDIR)
	@echo "Compiling $@"
	@$(CC) -c $< -o $@ $(CXXFLAGS)

